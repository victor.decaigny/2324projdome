# Table of Contents
1. [Intro](#intro)
2. [Git](#pulling-a-project-from-git-on-raspberry-pi)
3. [WireGuard](#setting-up-a-wireguard-vpn-on-the-raspberry-pi)
4. [FTP](#how-to-setup-ftp-on-the-raspberry-pi)
5. [Docker](#setting-up-docker-on-the-raspberry-pi)
6. [Mysql](#create-a-mysql-database-using-docker)
7. [Zigbee](#raspberry-pi-zigbee-data-script)
8. [DigitalMeter](#reading-data-from-the-belgian-digital-meter-using-the-p1-port)
9. [Mudblazor&Api](#mudblazor--api)

# Intro

The project aims to develop a robust system that simplifies the management of digital files and devices in the household while improving the security and accessibility of personal data, without transferring it to 'big tech'. The system is designed for users who are already familiar with the applicable IT technologies, with their expertise contributing to the continuous improvement of the system through an open-source approach. The project must meet a set of essential and optional features, including automatic device backup, advanced rights management, secure connections and remote access via VPN, as well as a smart mailbox interface, integration with local waste calendars and shared to-do list.

# Equipment List

## Recommended
- Raspberry Pi (with MicroPython)
- Micro SD Card
- Ethernet Cable or Wi-Fi
- Power Supply
- Zigbee module
- URM09 ultrasonic sensor

## Optional
- Raspberry Pi Case
- USB Keyboard
- USB Mouse
- HDMI Cable
- Monitor

# Pulling a Project from Git on Raspberry Pi

This tutorial will guide you through the process of pulling a project from a Git repository directly onto your Raspberry Pi. We'll be using the repository at `git@gitlab.com:ikdoeict/thibault.viaene/dome.git`.

## Prerequisites

- Ensure Git is installed on your Raspberry Pi. If not, you can install it by running:
```bash
   sudo apt update
   sudo apt install git
```

## Steps to Clone the Repository

### Step 1: Open Terminal

Open your terminal or command prompt.

### Step 2: Navigate to the Directory

Navigate to the directory where you want to clone the repository.

```bash
cd /path/to/your/directory
```
### Step 3: Clone the Repository

Run the following command to clone the repository using SSH:

```bash
git clone git@gitlab.com:ikdoeict/thibault.viaene/dome.git
```
### Step 4: Verify the Clone

Navigate into the cloned directory to verify the contents:

```bash
cd dome
ls
```

# Setting up a WireGuard VPN on the Raspberry Pi

## Introduction

This tutorial guides you through setting up a WireGuard VPN on a Raspberry Pi, providing a secure and fast VPN solution.

## Steps To Install WireGuard

### 1. Update System Packages Raspberry Pi 
```bash
   sudo apt update
   sudo apt full-upgrade
   sudo apt install curl -y
```

### 2. Install WireGuard using PiVPN Script
    
```bash
   curl -L https://install.pivpn.io | bash
```
  
### 3. Configure WireGuard through the installation prompts
   - Set a static IP
   - Choose the user for configuration storage
   - Select WireGuard as the VPN software
   - Configure port and DNS settings

### 4. Create a WireGuard Profile
   ```bash
   sudo pivpn add
   ```

## Additional Tips
- Ensure you have port forwarding enabled on your router.
- Use a public IP or domain for remote access.

For detailed steps and troubleshooting, refer to the full tutorial [here](https://pimylifeup.com/raspberry-pi-wireguard/).

# How to Setup FTP on the Raspberry Pi

We are going to take a look at setting up FTP on the Raspberry Pi. Setting up FTP will allow for easy transferring of files to and from your Pi over a network. There is also an additional method that you can be used instead. This method is called SFTP, it stands for Secure File Transfer Protocol. This version of the protocol works over SSH.

The biggest difference between these two methods comes down to their security. SFTP is considered to be much more secure thanks to its use of encryption for the transfer of data.

We should also note that it is possible to use both FTP and SFTP at the same time as they operate on difference ports. By default FTP operates on port 21, while SFTP operates on port 22.

## Enable SSH on the Raspberry Pi
All we need to do is make sure that SSH is enabled by using the Raspi-config tool. If you have already got SSH enabled, then you can skip ahead to the “How to Connect” section further down in this tutorial.

1. Open Terminal: `CTRL` + `ALT` + `T`
2. Enter: `sudo raspi-config`
3. Navigate to `5 Interfacing Options`
4. Select `P2 SSH` and enable it
5. Exit with `ESC`

## Installing and Configuring vsftpd for FTP
1. Update system packages:
    ```sh
    sudo apt update
    sudo apt full-upgrade
    ```
2. Install vsftpd:
    ```sh
    sudo apt install vsftpd
    ```
3. Configure vsftpd:
    ```sh
    sudo nano /etc/vsftpd.conf
    ```
    Add or uncomment the following lines:
    ```plaintext
    anonymous_enable=NO
    local_enable=YES
    write_enable=YES
    local_umask=022
    chroot_local_user=YES
    user_sub_token=$USER
    local_root=/home/$USER/FTP
    ```
4. Create FTP directory:
    ```sh
    mkdir -p /home/<user>/FTP/files
    chmod a-w /home/<user>/FTP
    ```
5. Restart vsftpd:
    ```sh
    sudo service vsftpd restart
    ```

## How to Connect to your Raspberry Pi’s FTP Server
1. Download and install FileZilla from the [FileZilla download page](https://filezilla-project.org/download.php).
2. Open FileZilla and enter the following details:
    1. **Host:** IP address of your Raspberry Pi.
    2. **Username:** `pi` (default user).
    3. **Password:** `raspberry` (default password).
    4. **Port:** `21` for FTP or `22` for SFTP.
3. Click `Quickconnect` to connect to your Raspberry Pi FTP server.
4. Test the connection by transferring a couple of files.


## Troubleshooting
- Permission issues: Adjust permissions for the required directory.

For more details, visit the [original tutorial](https://pimylifeup.com/raspberry-pi-ftp/).

# Setting Up Docker on the Raspberry Pi

## Introduction

Docker is an incredibly powerful tool that provides OS-level virtualization to deliver software packages within containers. It helps in deploying software efficiently, securely, and with low overhead, making it perfect for the Raspberry Pi.

## Steps For Settin up Docker

### Step 1: Update System Packages
Before installing Docker, ensure your system packages are up to date.
   ```
   sudo apt update
   sudo apt upgrade -y
   ```

### Step 2: Install Docker
Download and run the official Docker installation script.
   ```
   curl -sSL https://get.docker.com | sh
   ```

### Step 3: Add User to Docker Group
Allow your user to run Docker commands without sudo.
   ```
   sudo usermod -aG docker $USER
   logout
   ```
### Step 4: Test Docker Installation
Verify that Docker is installed correctly by running a test container.
   ```
   docker run hello-world
   ```

## Additional Tips
- For detailed steps and troubleshooting, refer to the full tutorial [here](https://pimylifeup.com/raspberry-pi-docker/).

# Docker Compose Setup Tutorial

## Introduction

This tutorial will guide you through setting up and running a Docker Compose that includes two services: an API service and a DomeBlazor service.

## Prerequisites

- Docker installed on your system
- Docker Compose installed on your system

## Directory Structure

Ensure your project directory is structured as follows:
```
    /project-folder
    /API
    Dockerfile
    /DomeBlazor
    Dockerfile
    docker-compose.yml
```

## Docker Compose File

Your `docker-compose.yml` file should look like this:

```yaml
services:
  api:
    build:
      context: .
      dockerfile: API/Dockerfile
    container_name: api
    ports:
      - "5000:80"

  domeblazor:
    build:
      context: .
      dockerfile: DomeBlazor/Dockerfile
    container_name: domeblazor
    ports:
      - "5003:80"
```

## API Docker File

Your 'Dockerfile' in the API folder should look like this:

```yaml
FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /src
COPY . .

WORKDIR /src/API
RUN dotnet restore
RUN dotnet publish -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS runtime
WORKDIR /app
COPY --from=build /app/publish .
ENTRYPOINT ["dotnet", "API.dll"]
``` 

## Blazor Docker File

Your 'Dockerfile' in the DomeBlazor folder should look like this:


```yaml
FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /src
COPY . .

WORKDIR /src/API
RUN dotnet restore
RUN dotnet publish -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS runtime
WORKDIR /app
COPY --from=build /app/publish .
ENTRYPOINT ["dotnet", "API.dll"]
``` 

## Running Docker Compose

```bash
docker compose up --build
```

# Create a MySQL Database Using Docker
MySQL is an open-source relational database owned by Oracle Corporation. The instructions we're going to share in this article demonstrate how to install and set up a MySQL server along with the popular phpMyAdmin management application.

## Step 1: Install Docker

First, ensure Docker is installed on your system. If it's not, you can install it by following the instructions on the official [Docker website](https://www.docker.com/).

## Step 2: Make  Docker Compose file

Copy the following definitions to a file named docker-compose.yml in your project directory:
```yml

services:
  db:
    image: mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: changethis!
    ports:
      - "3306:3306"
    volumes:
      - db-data:/var/lib/mysql
  admin:
    image: phpmyadmin
    restart: always
    environment:
      - PMA_ARBITRARY=1
    ports:
      - 8080:80
volumes:
  db-data:
```

This Docker Compose configuration file starts a service called db that runs a MySQL server connected to port 3306 of your computer, plus a second service called admin that runs phpMyAdmin on port 8080. The database storage is configured on a separate volume called db-data, to make it possible to upgrade the database container without losing data.

Note the MYSQL_ROOT_PASSWORD line, which has the value changethis!. This line defines the administrator password for the MySQL server. Edit this line to set a secure password of your liking.

Once you have this file saved in your project directory, return to the terminal and run the following command to start your MySQL server:

```bash
docker compose up -d
```

The first time you run this command it will take a while, as Docker has to download the MySQL and phpMyAdmin container images from the Docker Hub repository. Once the images are downloaded, it should take a few seconds for the containers to be launched, and at that point MySQL should be deployed on your computer and ready to be used.

## Configure phpMyAdmin

You can open the phpMyAdmin database management tool by typing http://localhost:8080 in the address bar of your web browser.

To log in, enter the following credentials:

- Server: db
- Username: root
- Password: the root password that you entered in the docker-compose.yml file

Once you access the phpMyAdmin interface, click on the "Databases" tab. Near the top you should see the "Create Database" section.

Enter a name for the new database, for example retrofun, and click the "Create" button.

A good practice when creating a new database is to also define a user specifically assigned to it. Using the root database user for day-to-day operations is too risky, because this account is too powerful and should only be used for important administration tasks.

Click on the "Privileges" tab for the new database. Near the bottom of the page there is a section titled "New" with an "Add user account" link. Click it to create a new user.

or the username you can choose any name that you like, but a naming convention that I find useful is to use the same name for the database and the user, so in this case it would be retrofun. Leave the host set to "%", then enter a password for the new user.

Confirm that the "Grant all privileges on database retrofun" option is enabled, and then scroll all the way to the bottom of the page and click the "Go" button to create the user. This user will have full access to the database, but it will not be able to access or create other databases, which is a good security principle to follow.

From now on, you can log in to phpMyAdmin using the user you just created, and your view of the database server will be constrained to only what's relevant to manage this particular database.

# Raspberry Pi Zigbee Data Script

## Introduction
This guide covers using a Python script on a Raspberry Pi to read data from a Zigbee-connected ultrasonic sensor and transmit it wirelessly.

## Configuration Steps

1. **Communication Encryption**

![ZigbeeSecurity](./ImagesReadMe/ZigbeeSecurity.png)

2. **Enable Micropython on Devices**

Set parameter `PS` to enabled.

![ZigbeeMicroPythonOption](./ImagesReadMe/ZigbeeMicroPythonOption.png)

3. **Network Setup**

Use a common PAN ID and channel for both devices (`CH` and `ID` parameters).

![ZigbeeNetworking](./ImagesReadMe/ZigbeeNetworking.png)

## Micropython Code: Reading and Transmitting Data

1. **Import Libraries:**

The script starts by importing the necessary libraries:

- `machine.I2C`: Enables I2C communication with the sensor.
- `time`: Provides time-related functions, such as delays.
- `xbee`: Enables wireless communication via the XBee module.


   ```python
   from machine import I2C
   import time
   import xbee
    ```

2. **Define Constants:**

- `URM09_ADDR`: The I2C address of the URM09 sensor (0x11).
- `DIST_HIGH` and `DIST_LOW`: Registers for reading the measured distance.
- `CONFIG_REG` and `COMMAND_REG`: Registers for configuring and controlling the sensor.
- `TARGET_64BIT_ADDR`: The 64-bit address of the target device to which data is sent.
- `i2c`: An I2C object created with a frequency of 100 kHz and a timeout of 2 seconds.


    ```python
    URM09_ADDR = 0x11
    DIST_HIGH = 0x03
    DIST_LOW = 0x04
    CONFIG_REG = 0x07
    COMMAND_REG = 0x08
    TARGET_64BIT_ADDR = b'\x00\x13\xA2\x00\x42\x48\xEA\x72'
    ```

3. **I2C Object**
    ```python
    i2c = I2C(1, freq=100000, timeout=2000)
    ```

4. **Read Distance Function**

This function reads the distance measured by the URM09 sensor via I2C communication. The steps are:

1. Set the sensor to passive measurement mode by writing to the configuration register.
2. Send a command to the sensor to measure the distance once.
3. Wait 50ms for the measurement to complete.
4. Read the high and low bytes of the measured distance from the respective registers.
5. Combine the bytes into a distance in centimeters.
6. Return the distance. If an error occurs, return `None` and print an error message.


    ```python
    def read_distance():
    try:
        config = 0b00000000
        i2c.writeto_mem(URM09_ADDR, CONFIG_REG, config.to_bytes(1, 'little'))
        i2c.writeto_mem(URM09_ADDR, COMMAND_REG, b'\x01')
        time.sleep_ms(50)
        dist_high = i2c.readfrom_mem(URM09_ADDR, DIST_HIGH, 1)[0]
        dist_low = i2c.readfrom_mem(URM09_ADDR, DIST_LOW, 1)[0]
        distance = (dist_high << 8) | dist_low
        return distance
    except OSError as e:
        print("Error reading distance: {}".format(str(e)))
        return None
    ```

5. **Main Loop:**

In the main part of the script, distances are continuously measured and transmitted:

1. Call the `read_distance()` function to measure the distance.
2. If the distance is read (not `None`), print it.
3. If the distance is less than or equal to 10 cm, send the message "1". Otherwise, send "0".
4. The message is sent wirelessly to the target device using the XBee module.
5. Print a confirmation on successful transmission. Print an error message on failure.
6. Wait 1 second before the next measurement.

    ```python
    while True:
    dist_cm = read_distance()
    if dist_cm is not None:
        print("Distance: {} cm".format(dist_cm))
        message = "1" if dist_cm <= 10 else "0"
        try:
            xbee.transmit(TARGET_64BIT_ADDR, message)
            print("Data sent: {}".format(message))
        except Exception as e:
            print("Transmission failed: %s" % str(e))
    else:
        print("Could not read distance.")
    time.sleep(1)
    ```
## Extending the script

1. **Deep Sleep Mode:**
## Enhancing Battery Life

By modifying the script, the mailbox status is read four times per hour. This method helps extend the battery life.

```python
def deep_sleep():
    sleep_pin.value(1)
    time.sleep(1)
    sleep_pin.value(0)

while True:
    deep_sleep()
    time.sleep(3600)
    for _ in range(4):
        dist_cm = read_distance()
        if dist_cm is not None:
            print("Distance: {} cm".format(dist_cm))
            message = "1" if dist_cm <= 10 else "0"
            try:
                xbee.transmit(TARGET_64BIT_ADDR, message)
                print("Data sent: {}".format(message))
            except Exception as e:
                print("Transmission failed: %s" % str(e))
        else:
            print("Could not read distance.")
        time.sleep(1)
```

![ZigbeeSleepSettings.png](./ImagesReadMe/ZigbeeSleepSettings.png)


## Coordinator Code

1. **Receive and Validate Data:**

```python
import xbee
print("Waiting for data...\n")
while True:
    received_msg = xbee.receive()
    if received_msg and 'payload' in received_msg:
        payload = received_msg['payload'].decode('utf-8')
        if payload in ['0', '1']:
            print(payload)
        else:
            print("Invalid payload.")
```

## Serial Monitor Logging
On the left side, the data sender logs the distance for monitoring purposes on the serial monitor. On the right side, the controller displays the received relevant data.

![ZibeeLoggingInXCTU](./ImagesReadMe/ZigbeeLoggingInXCTU.png)


## Micropython Code Coordinator
This code receives messages from the end device, checks if they are '0' or '1', and ensures no harmful effects if the communication is intercepted.

```python
import xbee

print(" +--------------------------------------+")
print(" | XBee MicroPython Receive Data Sample |")
print(" +--------------------------------------+\n")

print("Waiting for data...\n")

while True:
    # Check if the XBee has any message in the queue.
    received_msg = xbee.receive()
    if received_msg:
        # Ensure the 'payload' field exists and is not None before proceeding.
        if 'payload' in received_msg and received_msg['payload'] is not None:
            # Get the sender's 64-bit address and payload from the received message.
            sender = received_msg['sender_eui64']
            payload = received_msg['payload'].decode('utf-8')
            # Check if the payload is '0' or '1'
            if payload in ['0', '1']:
                print(payload)
            else:
                print("Received a message without a payload.")

This section logs the sent and received data for monitoring and includes the coordinator code that ensures the integrity and safety of the communication.
```

# Reading Data from the Belgian Digital Meter Using the P1 Port

## Introduction

- Digital electricity meters are being used in various countries
- In Belgium, Fluvius has started the distribution and installation of digital meters
- Currently, three types are being used for consumers:
  - Sagecom S211 (single-phase)
  - Sagecom T211-D (three-phase)
  - Flonidan – G4SRTV (natural gas)
**Connection Cable**: RJ11/RJ12 with a serial to USB converter.

## The P1 and S1 Ports

- Electricity meters have two serial ports on the front
- Both ports can be used to read data
- S1 port sends raw data, around 2600-4000 times/second
- P1 port provides formatted data approximately once per second

## Connecting to the P1 Port

- RJ12 connector needed to connect
- Pinout is standardized
- Specific USB cables available to connect directly to a computer
- Ensure the correct permissions for the serial port:
```sh
sudo apt install python3-serial python3-crcmod python3-tabulate
```
- To use the serial port as a non-root user, you need to set the correct permissions first:
```sh
sudo chmod o+rw /dev/ttyUSB0
```
- You can read the data using Python's serial tools:
```sh
python3 -m serial.tools.miniterm /dev/ttyUSB0 115200 --xonxoff
```

## Data Format

- Data is sent in telegrams
- Each line contains an OBIS reference followed by the data
- Examples of OBIS codes and their meanings are provided

The P1 port uses the DSMR (Dutch Smart Meter Requirements) 5.0.2 P1 standard. Each data block (telegram) contains multiple OBIS codes representing various measurements.

```makefile
/FLU5\253xxxxxx_A
0-0:96.1.4(xxxxx)
0-0:96.1.1(xxxxxxxxxxxxxxxxxxxxxxxxxxxx)
...
```

## OBIS Codes
Some common OBIS codes include:

- 1-0:1.8.1: Total consumption during the day
- 1-0:2.8.1: Total production during the day
- 0-0:96.14.0: Current tariff (1=day, 2=night)
- 1-0:32.7.0: Voltage on phase L1

For a detailed list of OBIS codes and their meanings, refer to the Fluvius technical info.

## Python Script

- Example Python script to parse the data
```python
import serial

serialport = '/dev/ttyUSB0'
baudrate = 115200

ser = serial.Serial(serialport, baudrate, xonxoff=True)
while True:
    line = ser.readline().decode('ascii').strip()
    print(line)
```

- Extracts key information from the data such as:
  - Meter serial numbers
  - Consumption and injection per tariff
  - Current consumption and injection
  - Voltage, current, and switch positions

  Save the script as read_p1.py and run it:
```sh
python3 read_p1.py

```

You can further process the data to display it in a readable format using Python. Here's an extended script:

```python
import serial
import crcmod
from tabulate import tabulate

serialport = '/dev/ttyUSB0'
baudrate = 115200

def read_p1_data():
    ser = serial.Serial(serialport, baudrate, xonxoff=True)
    data = []
    while True:
        line = ser.readline().decode('ascii').strip()
        if line:
            data.append(line)
            if line.startswith('!'):
                break
    return data

def parse_p1_data(data):
    parsed_data = {}
    for line in data:
        if line.startswith('1-0:1.8.1'):
            parsed_data['Rate 1 (day) - total consumption'] = line.split('(')[1].split('*')[
```


## Additional Resources

The tutorial provides a good starting point for anyone who wants to get started with reading data from their Belgian digital meter via the P1 port.

For more details and to access the complete script, visit the [GitHub repository](https://github.com/jensdepuydt/belgian_digitalmeter_p1).


# Mudblazor & API
This project provides a foundation built with MudBlazor for UI components, .NET Fluent API for building APIs, and Microsoft Identity for authentication and authorization. Users are encouraged to implement their own features and customize the application to meet their specific needs.

## Technologies Used

- **MudBlazor**: A Blazor component library for modern web applications.
- **.NET Fluent API**: A Fluent API for building .NET applications.
- **Microsoft Identity**: Authentication and authorization for .NET applications.

## Prerequisites

Before you begin, ensure you have the following installed on your local machine:

- [.NET 6 SDK](https://dotnet.microsoft.com/download/dotnet/6.0)
- [Visual Studio 2022](https://visualstudio.microsoft.com/) or [Visual Studio Code](https://code.visualstudio.com/)
- [Node.js](https://nodejs.org/) (for frontend dependencies)

## Getting Started

### Extending MudBlazor

The MudBlazor components are already integrated into the application. To implement your own features:

1. **Create New Components**:
   You can create new components by extending existing MudBlazor components or creating entirely new ones. For example, to create a custom button:
    ```razor
    <MudButton Variant="Variant.Filled" Color="Color.Secondary">Custom Button</MudButton>
    ```

2. **Modify Existing Components**:
   Customize existing components in your Razor pages or components:
    ```razor
    <MudTextField @bind-Value="myValue" Label="Enter Value" Variant="Variant.Outlined" />
    ```

### Extending .NET Fluent API

The API setup with .NET Fluent API is ready for use. To add new features:

1. **Add New Models**:
   Define new models as needed:
    ```csharp
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
    ```

2. **Extend DbContext**:
   Update the `ApplicationDbContext` to include new entities:
    ```csharp
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().Property(p => p.Name).IsRequired();
            // Additional configurations
        }
    }
    ```

3. **Create New API Endpoints**:
   Add new controllers and endpoints to handle API requests:
    ```csharp
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ProductsController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetProducts()
        {
            return await _context.Products.ToListAsync();
        }
        
        // Additional endpoints
    }
    ```

### Extending Microsoft Identity

The Microsoft Identity setup is ready to manage authentication and authorization. To customize it further:

1. **Customize Identity Models**:
   Extend the Identity models to include additional properties:
    ```csharp
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
    }
    ```

2. **Update Identity Configuration**:
   Configure Identity to use the custom user model in `Program.cs` or `Startup.cs`:
    ```csharp
    builder.Services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
        .AddEntityFrameworkStores<ApplicationDbContext>();
    ```

3. **Implement Custom Authorization**:
   Create custom authorization policies:
    ```csharp
    builder.Services.AddAuthorization(options =>
    {
        options.AddPolicy("AdminOnly", policy => policy.RequireRole("Admin"));
    });
    ```

## Running the Application

1. **Build the Project**:
    ```sh
    dotnet build
    ```

2. **Run the Application**:
    ```sh
    dotnet run
    ```

3. **Access the Application**:
    Open your browser and navigate to `https://localhost:5001`.

## Additional Resources

- [MudBlazor Documentation](https://mudblazor.com/)
- [.NET Fluent API Documentation](https://docs.microsoft.com/en-us/ef/core/modeling/)
- [Microsoft Identity Platform Documentation](https://docs.microsoft.com/en-us/azure/active-directory/develop/)

# Contributing

Contributions are welcome! Please open an issue or submit a pull request for any improvements.


