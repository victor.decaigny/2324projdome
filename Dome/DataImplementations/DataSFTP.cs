﻿using FluentFTP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Globals;

namespace DataImplementations
{
    public class DataSFTP
    {
        private readonly string _host = "192.168.1.253";
        private readonly string _username = "sftpuser";
        private readonly string _password = "sftpUs3r";

        public async Task<List<SftpFile>> ListFilesAsync(string remoteDirectory)
        {
            var files = new List<SftpFile>();
            var client = new FtpClient(_host, _username, _password);

            try
            {
                client.Connect();

                var items = client.GetListing(remoteDirectory, FtpListOption.AllFiles);

                foreach (var item in items)
                {
                    if (!item.Name.StartsWith("."))
                    {
                        files.Add(new SftpFile
                        {
                            Name = item.Name,
                            FullPath = item.FullName,
                            Size = item.Size,
                            IsDirectory = true,
                            IsPreviewable = IsPreviewable(item.Name)
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
            finally
            {
                client.Disconnect();
            }

            return files;
        }

        private bool IsPreviewable(string fileName)
        {
            var previewableExtensions = new HashSet<string> { ".txt", ".pdf", ".jpg", ".png" };
            return previewableExtensions.Contains(Path.GetExtension(fileName).ToLowerInvariant());
        }
    }
}