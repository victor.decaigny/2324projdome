﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals
{
	public class Address
	{
		public int Id { get; set; }
		public string IvagoStreet { get; set; }
		public string Number { get; set; }
	}
}
