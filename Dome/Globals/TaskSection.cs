﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Globals
{
    public class TaskSection
    {
        public Guid Id { get; set; }
        public string Name { get; init; }
        public bool NewTaskOpen { get; set; }
        public string NewTaskName { get; set; }
		public Guid UserId { get; set; }

		[JsonPropertyName("getTaskItemModels")]
        public List<TaskItem> getTaskItemModels { get; set; } = new List<TaskItem>();
    }
}
