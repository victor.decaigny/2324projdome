﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals
{
	public class MailItem
	{
		public int Id { get; set; }
		public bool HasMail { get; set; }
		public DateTime Timestamp { get; set; }
	}
}
