﻿using System.ComponentModel.DataAnnotations;

namespace API.Entities
{
    public class TaskSection
    {
        [Required]
        [RegularExpression(@"(?im)^[{(]?[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?$")]
        public Guid Id { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Name { get; set; }

        public virtual List<TaskItem> TaskItems { get; set; }
        public virtual User User { get; set; }
        public virtual Guid UserId { get; set; }

    }
}
