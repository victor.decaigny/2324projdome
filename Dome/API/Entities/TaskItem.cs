﻿using System.ComponentModel.DataAnnotations;

namespace API.Entities
{
    public class TaskItem
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Name { get; set; }


        public virtual Guid TaskSectionId { get; set; }
        public virtual TaskSection TaskSection { get; set; }
    }
}
