﻿using API.Entities;
using Globals;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories
{
	public class MailRepository : IMailRepository
	{
		private readonly DataBaseContext _context;
		public MailRepository(DataBaseContext context)
		{
			_context = context;
		}
		public async Task<MailItem> GetMail()
		{
			return await _context.mail
			   .Select(x => new MailItem
			   {
				   Id = x.Id,
				   HasMail = x.HasMail,
				   Timestamp = x.Timestamp
			   })
			   .AsNoTracking()
			   .FirstOrDefaultAsync();
		}
	}
}
