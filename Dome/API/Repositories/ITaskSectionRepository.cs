﻿using Globals.Models.TaskSection;
using Globals.Models.User;

namespace API.Repositories
{
    public interface ITaskSectionRepository
    {
        Task<List<GetTaskSectionModel>> GetTaskSections();
        Task<List<GetTaskSectionModel>> GetTaskSection(Guid id);
        Task<GetTaskSectionModel> PostTaskSection(PostTaskSectionModel postTaskSectionModel);
        Task PutTaskSection(Guid id, PutTaskSectionModel postTaskSectionModel);
        Task<string> DeleteTaskSection(Guid id);
    }
}
