﻿using Microsoft.AspNetCore.Identity;
using Globals.Models.User;
using Globals.Models.User;

namespace API.Repositories
{
    public interface IUserRepository
    {
        Task<List<GetUserModel>> GetUsers();
        Task<GetUserModel> GetUser(Guid id);
        Task<GetUserModel> PostUser(PostUserModel postUserModel);
        Task<bool> PutUser(string id, PutUserModel putUserModel);
        Task PatchUser(string id, PatchUserModel patchUserModel);
        Task<IdentityResult> DeleteUser(string id);
        Task<PostAuthenticationResponseModel> Authenticate(PostAuthenticationRequestModel postAuthenticationRequestModel, string ipAddress);
        Task<PostAuthenticationResponseModel> RenewToken(string token, string ipAddress);
        Task DeactivateToken(string token, string ipAddress);

    }
}
