﻿using API.Entities;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.EntityFrameworkCore;
using Globals.Models.TaskItem;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Components;
using System.IO;
using System.Runtime.CompilerServices;

namespace API.Repositories
{
    public class TaskItemRepository : ITaskItemRepository
    {
        private readonly DataBaseContext _context;

        public TaskItemRepository(DataBaseContext context)
        {
            _context = context;
        }
        public async Task<string> DeleteTaskItem(Guid id)
        {
			try
			{
				TaskItem? taskItem = await _context.TaskItems.FindAsync(id);
				_context.Remove(taskItem);
				await _context.SaveChangesAsync();
                return "y";
			}
			catch (Exception ex)
			{
				return ex.Message;
			}

        }

        public async Task<GetTaskItemModel> GetTaskItem(Guid id)
        {
            GetTaskItemModel? taskItem = await _context.TaskItems
                .Select(x => new GetTaskItemModel { 
                    Id = x.Id,
                    Name = x.Name
                    
                }).AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            return taskItem;
        }

        public async Task<List<GetTaskItemModel>> GetTaskItems()
        {
            return await _context.TaskItems
                .Select(x => new GetTaskItemModel
                {
                    Id = x.Id,
                    Name = x.Name

                }).AsNoTracking()
                .ToListAsync();
        }

        public async Task PatchTaskItem(string id, PatchTaskItemModel patchTaskItemModel)
        {
            TaskItem? taskItem = await _context.TaskItems.FindAsync(id);
            taskItem.Name = patchTaskItemModel.Name;
            _context.Update(taskItem);
            await _context.SaveChangesAsync();
        }

        public async Task<GetTaskItemModel> PostTaskItem(PostTaskItemModel postTaskItemModel)
        {
            TaskItem taskItem = new TaskItem
            {
                Name = postTaskItemModel.Name,
                TaskSectionId = postTaskItemModel.TaskSectionId
            };

            await _context.TaskItems.AddAsync(taskItem);
            await _context.SaveChangesAsync();

            return new GetTaskItemModel
            {
                Id = taskItem.Id,
                Name = taskItem.Name
            };
        }

        public async Task PutTaskItem(Guid id, PutTaskItemModel postTaskItemModel)
        {
            TaskItem? taskItem = await _context.TaskItems.FindAsync(id);
            taskItem.Name = postTaskItemModel.Name;
            _context.Update(taskItem);
            await _context.SaveChangesAsync();
        }
    }
}
