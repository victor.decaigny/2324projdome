﻿using API.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Globals.Models.Role;
using Globals.Models.User;

namespace API.Controllers
{
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly IRoleRepository _roleRepository;

        public RolesController(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        [HttpGet(Name = "GetRoles")]
        //[Authorize(Roles = "admin")]
        public async Task<List<GetRoleModel>> Get()
        {
            return await _roleRepository.GetRoles();
        }

        [HttpGet("{id}")]
        //[Authorize(Roles = "admin")]
        public async Task<GetRoleModel> Get(string id)
        {
            return await _roleRepository.GetRole(Guid.Parse(id));
        }

        [HttpPut(Name = "PutRole")]
        //[Authorize(Roles = "admin")]
        public async void Put(string id, PutRoleModel putRoleModel)
        {
            await _roleRepository.PutRole(id, putRoleModel);
        }

        [HttpPost(Name = "PostRole")]
        //[Authorize(Roles = "admin")]
        public async void Post(string id, PostRoleModel postRoleModel)
        {
            await _roleRepository.PostRole(postRoleModel);
        }

        [HttpPatch(Name = "PatchRole")]
        //[Authorize(Roles = "admin")]
        public async void Patch(string id, PatchRoleModel patchRoleModel)
        {
            await _roleRepository.PatchRole(id, patchRoleModel);
        }
    }
}

