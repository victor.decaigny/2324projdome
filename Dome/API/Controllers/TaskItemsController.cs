﻿using API.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Globals.Models.TaskItem;
using Globals.Models.User;

namespace API.Controllers
{
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [ApiController]
    public class TaskItemsController : ControllerBase
    {
        private readonly ITaskItemRepository _taskItemRepository;

        public TaskItemsController(ITaskItemRepository taskItemRepository)
        {
            _taskItemRepository = taskItemRepository;
        }

        //[Authorize(Roles = "admin")]
        [HttpGet(Name = "GetTaskitems")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<List<GetTaskItemModel>> Get()
        {
            return await _taskItemRepository.GetTaskItems();

        }

        [HttpGet("{id}")]
        //[Authorize(Roles = "admin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<GetTaskItemModel> Get(string id)
        {
            return await _taskItemRepository.GetTaskItem(Guid.Parse(id));
        }

        [HttpPut(Name = "PutTaskItem")]
        //[Authorize(Roles = "admin,user")]
        public async Task Put(string id, PutTaskItemModel putTaskItemModel)
        {
            await _taskItemRepository.PutTaskItem(Guid.Parse(id), putTaskItemModel);
            
        }

        [HttpPost(Name = "PostTaskItem")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        //[Authorize(Roles = "admin,user")]
        public async Task<ActionResult<GetTaskItemModel>> Post(PostTaskItemModel postTaskItemModel)
        {
            GetTaskItemModel getTaskItemModel = await _taskItemRepository.PostTaskItem(postTaskItemModel);
            return getTaskItemModel;
        }

		[HttpDelete("{id}")]
		//[Authorize(Roles = "admin")]
		public async Task<IActionResult> Delete(string id)
		{
            string response = await _taskItemRepository.DeleteTaskItem(Guid.Parse(id));

            if (response.Equals("y"))
            {
                return Ok("Task deleted succesfully");
            }
            return StatusCode(500, $"Internal server error: {response}");
		}
	}
}

