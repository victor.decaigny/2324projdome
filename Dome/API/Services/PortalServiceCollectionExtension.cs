﻿using API.Repositories;
using API.Helpers;

namespace API.Services
{
    public static class PortalServiceCollectionExtension
    {
        public static IServiceCollection AddPortalServices(
          this IServiceCollection services, IConfiguration config)
        {
            //services.AddTransient<AppSettings>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<ITaskItemRepository, TaskItemRepository>();
			services.AddTransient<ITaskSectionRepository, TaskSectionRepository>();
			services.AddTransient<IDoorheenDeDagVerbruikRepository, DoorheenDeDagVerbruikRepository>();
			services.AddTransient<IMailRepository, MailRepository>();
			services.AddTransient<IAddressRepository, AddressRepository>();
			services.AddTransient<IGarbageRepository, GarbageRepository>();


			return services;
        }
    }
}