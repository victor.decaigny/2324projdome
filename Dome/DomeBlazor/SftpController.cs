﻿using DataImplementations;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace DomeBlazor
{ 
    [ApiController]
    [Route("[controller]")]
    public class FtpController : ControllerBase
    {
        private readonly DataSFTP _ftpService = new DataSFTP();

        [HttpGet("list-files")]
        public async Task<IActionResult> ListFiles(string path)
        {
            try
            {
                var files = await _ftpService.ListFilesAsync(path);
                return Ok(files);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }
    }
}
